package com.nic.rojgar

import android.app.Application


/**
 * Created by Akash Singhal on 07-Dec-21.
 * Mail Id: akashsi126@gmail.com
 */
class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: MyApplication
        private set
    }
}