package com.nic.rojgar.data.response

data class Signuprequest(
    val CountryCode: String,
    val Email: String,
    val IpAddress: String,
    val Location: String,
    val Mobile: String,
    val Name: String,
    val Password: String,
    val isTerms: Boolean,
    val oneSignalId: String
)