package com.nic.rojgar.data.repository

import com.nic.ewam.data.network.AuthApi
import com.nic.rojgar.data.UserPreferences
import com.nic.rojgar.data.response.Signuprequest


/**
 * Created by Akash Singhal on 06-Dec-21.
 * Mail Id: akashsi126@gmail.com
 */
class RegistrationRepository(
    private val api: AuthApi,
    private val userPreferences: UserPreferences
) : BaseRepository() {

    suspend fun signUp(
        item: Signuprequest,
    ) = safeApiCall {
        api.signUp(
            item
        )
    }
}