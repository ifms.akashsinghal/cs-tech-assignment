package com.nic.rojgar.data.response

data class UserInfoResponse(
    val Data: Any,
    val Message: Any,
    val Package_type: String,
    val PayPalSubscriptionId: Any,
    val Redial: Any,
    val StatusCode: String,
    val SubscriptionCancelDate: Any,
    val SubscriptionExpiryDate: Any,
    val TodaysCalls: Any,
    val parentAccount: Any,
    val subscriptionType: Any,
    val userMobile: String,
    val userName: String,
    val userType: String,
    val userid: String
)