package com.nic.ewam.data.network

import com.nic.rojgar.data.response.RegistrationResponse
import com.nic.rojgar.data.response.UserInfoResponse
import com.nic.rojgar.data.response.Signuprequest
import retrofit2.http.*

/**
 * Created by Mormukut Singh on 09/08/2021.
 */

interface AuthApi {

    @GET("service/userloginV1")
    suspend fun login(
        @Query("emailid") emailId: String,
        @Query("password") password: String,
        @Query("oneSignalId") oneSignalId: String,
    ): UserInfoResponse

    @POST("service/Signup")
    suspend fun signUp(
        @Body request: Signuprequest,
    ): RegistrationResponse

}