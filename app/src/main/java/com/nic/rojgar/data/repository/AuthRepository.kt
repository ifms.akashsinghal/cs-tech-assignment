package com.nic.rojgar.data.repository

import com.nic.ewam.data.network.AuthApi
import com.nic.rojgar.data.UserPreferences
import com.nic.rojgar.data.response.UserInfoResponse


/**
 * Created by Akash Singhal on 18-Nov-21.
 * Mail Id: akashsi126@gmail.com
 */
class AuthRepository(
    private val api: AuthApi,
    private val userPreferences: UserPreferences
) : BaseRepository() {

    suspend fun login(
        email: String,
        password: String,
        oneSignalId: String,
    ) = safeApiCall {
        api.login(email, password, oneSignalId)
    }

    suspend fun saveAuthToken(token: String) {
        userPreferences.saveAccessTokens(token)
    }

    suspend fun putString(key: String, value: String) {
        userPreferences.putString(key, value)
    }

    suspend fun saveUserProfile(value: UserInfoResponse) {
        userPreferences.saveUserProfile(value)
    }
}