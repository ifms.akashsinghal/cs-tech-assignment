package com.nic.rojgar.data.response

data class RegistrationResponse(
    val Data: String,
    val Message: String,
    val Package_type: String,
    val PayPalSubscriptionId: String,
    val Redial: String,
    val StatusCode: String,
    val SubscriptionCancelDate: String,
    val SubscriptionExpiryDate: String,
    val TodaysCalls: String,
    val parentAccount: String,
    val subscriptionType: String,
    val userMobile: String,
    val userName: String,
    val userType: String,
    val userid: String
)