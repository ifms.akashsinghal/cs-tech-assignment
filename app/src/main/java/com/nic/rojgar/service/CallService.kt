package com.nic.rojgar.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import com.nic.rojgar.activity.AlertActivity
import java.lang.Exception
import com.nic.rojgar.MyApplication


class CallService : BroadcastReceiver() {




    override fun onReceive(context: Context?, intent: Intent?) {
        try {
            System.out.println("Receiver start")
            Log.e("Call Service", intent.toString())
            Toast.makeText(context, " Receiver start ", Toast.LENGTH_SHORT).show()

            var state: String = intent!!.getStringExtra(TelephonyManager.EXTRA_STATE)!!
            val incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)

//            var extras  = intent.extras
//            var intent: Intent = Intent(context, AlertActivity::class.java)
//            context?.startActivity(intent)
            if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                Toast.makeText(context,"Ringing State Number is -"+incomingNumber,Toast.LENGTH_SHORT).show()
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"))
//                browserIntent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
//                MyApplication.instance.startActivity(browserIntent)

            }
            if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))){
                Toast.makeText(context,"Received State",Toast.LENGTH_SHORT).show()
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                Toast.makeText(context,"Idle State",Toast.LENGTH_SHORT).show()
            }

            val i = Intent(context,AlertActivity::class.java)
            i.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            MyApplication.instance.sendBroadcast(i)
            MyApplication.instance.applicationContext.startActivity(i)
            MyApplication.instance.startService(i)


        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}