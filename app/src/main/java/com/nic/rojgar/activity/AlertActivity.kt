package com.nic.rojgar.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentUris
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.telephony.TelephonyManager
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.nic.rojgar.R
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.lang.NumberFormatException
import java.lang.reflect.Method

class AlertActivity : AppCompatActivity() {
    var x1 = 0f
    var x2:kotlin.Float = 0f
    val MIN_DISTANCE = 150
    var Cname: TextView? = null
    var Cnumber:TextView? = null
    var ContactName: String? =
        null
    var Contactnumber:kotlin.String? = null
    var id:kotlin.String? = null
    var cid: Long = 0
    var Cphoto: ImageView? = null
    var mActivity: Activity? = null
    var card: RelativeLayout? = null
    var rslide: Animation? = null
    var lslide:Animation? = null
    private var wind: Window? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setFinishOnTouchOutside(true)
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_alert)
            mActivity = this
            initializeContent()
            ContactName = intent.extras!!.getString("contact_name")
            Contactnumber = intent.extras!!.getString("phone_no")
            id = intent.extras!!.getString("id")
            try {
                if (id != null) {
                    cid = id!!.toLong()
                    retrieveContactPhoto(cid)
                }
            } catch (e: NumberFormatException) {
                println("nfe")
            }
            if (ContactName == null) {
                Cname!!.text = "Unknown number"
                Cnumber!!.text = Contactnumber
            } else {
                Cname!!.text = "$ContactName is calling you"
                Cnumber!!.text = Contactnumber
            }
        } catch (e: Exception) {
            Log.d("Exception", e.toString())
            e.printStackTrace()
        }
        card = findViewById<View>(R.id.card) as RelativeLayout
    }

    private fun initializeContent() {
        Cname = findViewById<View>(R.id.Cname) as TextView
//        Cnumber = findViewById<View>(R.id.number) as TextView
        Cphoto = findViewById<View>(R.id.contactPhoto) as ImageView
    }

    private fun retrieveContactPhoto(contactID: Long) {
        var photo: Bitmap? = null
        try {
            val inputStream: InputStream = ContactsContract.Contacts.openContactPhotoInputStream(
                contentResolver,
                ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactID)
            )
            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream)
                Cphoto!!.setImageBitmap(photo)
            }
            if (inputStream != null) inputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SoonBlockedPrivateApi")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.getAction()) {
            MotionEvent.ACTION_DOWN -> x1 = event.getX()
            MotionEvent.ACTION_UP -> {
                x2 = event.getX()
                val deltaX = x2 - x1
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    if (x2 > x1) {
                        card!!.startAnimation(rslide)
                        Handler().postDelayed(Runnable {
                            this.finish()
                            System.exit(0)
                            ExitActivity().exitApplication(applicationContext)
                        }, 500)
                    } else {
                        card!!.startAnimation(lslide)
                        Handler().postDelayed(Runnable {
                            try {
                                val tm: TelephonyManager =
                                    getSystemService(TELEPHONY_SERVICE) as TelephonyManager
                                val m1: Method = tm.javaClass.getDeclaredMethod("getITelephony")
                                m1.setAccessible(true)
                                val iTelephony: Any = m1.invoke(tm)
                                val m2: Method =
                                    iTelephony.javaClass.getDeclaredMethod("silenceRinger")
                                val m3: Method = iTelephony.javaClass.getDeclaredMethod("endCall")
                                m2.invoke(iTelephony)
                                m3.invoke(iTelephony)
                            } catch (e: Exception) {
                            }
                            this.finish()
                            System.exit(0)
                            ExitActivity().exitApplication(applicationContext)
                        }, 500)
                    }
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onResume() {
        // TODO Auto-generated method stub
        super.onResume()
        wind = this.window
//        wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)
//        wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
//        wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
//        wind.addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
//        wind.addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY)
    }

}