package com.nic.rojgar.fragments.home

import android.provider.CallLog
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.nic.rojgar.databinding.CallLogsListBinding


/**
 * Created by Akash Singhal on 09-Dec-21.
 * Mail Id: akashsi126@gmail.com
 */
class CallLogAdapter(val requireActivity: FragmentActivity,val from: Array<String>) :
  RecyclerView.Adapter<CallLogAdapter.ViewHolder>(){



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            CallLogsListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding,requireActivity)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(from[position],requireActivity)
    }

    override fun getItemCount(): Int = from.size


        class ViewHolder(val ItemView: CallLogsListBinding, var context: FragmentActivity) : RecyclerView.ViewHolder(ItemView.root){
          fun bind(
              from: String,
              context: FragmentActivity
          ){
              ItemView.tv1.text = CallLog.Calls.NUMBER
              ItemView.tv2.text = CallLog.Calls.DURATION
              ItemView.tv3.text = CallLog.Calls.TYPE
        }

    }
}