package com.nic.rojgar.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nic.ewam.data.network.Resource
import com.nic.rojgar.data.repository.RegistrationRepository
import com.nic.rojgar.data.response.RegistrationResponse
import com.nic.rojgar.data.response.Signuprequest
import com.nic.rojgar.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class RegistrationViewModel(
    private val repository: RegistrationRepository
) : BaseViewModel(repository) {
    private val _signUpResponse: MutableLiveData<Resource<RegistrationResponse>> = MutableLiveData()

    val signUpResponse: LiveData<Resource<RegistrationResponse>>
        get() = _signUpResponse

    fun signUp(
        item : Signuprequest,
    ) = viewModelScope.launch {
        _signUpResponse.value = Resource.Loading
        _signUpResponse.value = repository.signUp(item)

    }
}