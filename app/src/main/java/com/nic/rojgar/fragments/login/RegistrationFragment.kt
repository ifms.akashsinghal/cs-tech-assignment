package com.nic.rojgar.fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.nic.ewam.data.network.AuthApi
import com.nic.ewam.data.network.Resource
import com.nic.rojgar.data.repository.RegistrationRepository
import com.nic.rojgar.data.response.RegistrationResponse
import com.nic.rojgar.data.response.Signuprequest
import com.nic.rojgar.databinding.RegistrationFragmentBinding
import com.nic.rojgar.ui.base.BaseFragment
import com.nic.rojgar.ui.enable
import com.nic.rojgar.ui.handleApiError
import com.nic.rojgar.ui.visible

class RegistrationFragment :
    BaseFragment<RegistrationViewModel, RegistrationFragmentBinding, RegistrationRepository>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.signUpResponse.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Success -> {
                    updateUi(it.value)
                }
                is Resource.Failure -> {
                    handleApiError(it) {
//                        callSignUpApi()
                    }
                }
            }
        })

        binding.btnSubmit.setOnClickListener {
//            callSignUpApi()
        }

        binding.btnBack.setOnClickListener(View.OnClickListener {
            showPreView()
        })

        binding.btnSubmit.setOnClickListener(View.OnClickListener {
            showNextView()
        })

        showFirstView()
    }

    var position = 0
    private fun showNextView() {
        position++
        openPage()
    }

    private fun showPreView() {
        position--
        openPage()
    }

    private fun openPage() {
        when (position) {
            0 ->
                showFirstView()
            1 ->
                showSecondView()
            2 ->
                showThirdView()
            3 ->
                showForthView()
            else -> {
                position = 0
                showFirstView()
            }
        }
    }

    private fun showForthView() {
        binding.included1.firstView.visible(false)
        binding.included2.secondView.visible(false)
        binding.included3.thirdView.visible(false)
        binding.included4.forthView.visible(true)
        binding.btnBack.enable(true)
        binding.btnSubmit.enable(false)
    }

    private fun showThirdView() {
        binding.included1.firstView.visible(false)
        binding.included2.secondView.visible(false)
        binding.included3.thirdView.visible(true)
        binding.included4.forthView.visible(false)
        binding.btnBack.enable(true)
        binding.btnSubmit.enable(true)
    }

    private fun showSecondView() {
        binding.included1.firstView.visible(false)
        binding.included2.secondView.visible(true)
        binding.included3.thirdView.visible(false)
        binding.included4.forthView.visible(false)
        binding.btnBack.enable(true)
        binding.btnSubmit.enable(true)
    }

    private fun showFirstView() {
        binding.included1.firstView.visible(true)
        binding.included2.secondView.visible(false)
        binding.included3.thirdView.visible(false)
        binding.included4.forthView.visible(false)
        binding.btnBack.enable(false)
        binding.btnSubmit.enable(true)
    }

    private fun updateUi(value: RegistrationResponse) {
        Toast.makeText(requireContext(), value.Message, Toast.LENGTH_SHORT).show()
    }

    lateinit var item: Signuprequest
//    private fun callSignUpApi() {
//        var name = binding.etName.text.toString()
//        var email = binding.etUsername.text.toString()
//        var mobile = binding.etMobile.text.toString()
//        var countryCode = binding.etCountryCode.text.toString()
//        var password = binding.etPassword.text.toString()
//        var checked = binding.checkBox.isChecked
//        var location = "2.35687458,31.654899"
//
//        item = Signuprequest(countryCode, email, "", location, mobile, name, password, checked, "0")
//
//        viewModel.signUp(item)
//
////        viewModel.signUp(
////            name.toString(),
////            mobile.toString(),
////            email.toString(),
////            password.toString(),
////            countryCode.toString(),
////            checked,
////            "",
////            "0",
////            location
////        )
//
//    }


    override fun getViewModel() = RegistrationViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = RegistrationFragmentBinding.inflate(inflater, container, false)

    override fun getFragmentRepository() =
        RegistrationRepository(remoteDataSource.buildApi(AuthApi::class.java), userPreferences)

}