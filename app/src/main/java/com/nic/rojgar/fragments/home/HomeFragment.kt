package com.nic.rojgar.fragments.home

import android.os.AsyncTask
import android.os.Bundle
import android.provider.CallLog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.nic.rojgar.R
import com.nic.rojgar.data.UserPreferences
import com.nic.rojgar.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null


    var calls = listOf<String>(
        CallLog.Calls._ID,
        CallLog.Calls.NUMBER,
        CallLog.Calls.TYPE,
        CallLog.Calls.DURATION
    ).toTypedArray()


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        displayLog()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var userPerfrence: UserPreferences = UserPreferences(requireContext())


//        binding.tvUserInfo.text = runBlocking { userPerfrence.getUserProfile().toString() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun displayLog() {
        var from = listOf<String>(
            CallLog.Calls.NUMBER,
            CallLog.Calls.DURATION,
            CallLog.Calls.TYPE
        ).toTypedArray()

        var to = intArrayOf(R.id.tv1, R.id.tv2, R.id.tv3)

        var rs =
            requireContext().contentResolver.query(
                CallLog.Calls.CONTENT_URI, calls, null, null,
                "${CallLog.Calls.LAST_MODIFIED} DESC"
            )

        Log.e("fragment", rs.toString())

        var adapter = SimpleCursorAdapter(
            activity?.applicationContext, R.layout.call_logs_list,
            rs, from, to, 0
        )
        binding.listView.adapter = adapter

//        binding.listView.layoutManager = LinearLayoutManager(activity)
//        binding.listView.adapter =
//            CallLogAdapter(requireActivity(), from)
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }
}