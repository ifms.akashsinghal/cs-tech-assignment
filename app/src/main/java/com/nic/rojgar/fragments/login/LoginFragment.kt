package com.nic.rojgar.fragments.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.nic.ewam.data.network.AuthApi
import com.nic.ewam.data.network.Resource
import com.nic.rojgar.R
import com.nic.rojgar.activity.MainActivity
import com.nic.rojgar.data.UserPreferences
import com.nic.rojgar.data.repository.AuthRepository
import com.nic.rojgar.databinding.LoginFragmentBinding
import com.nic.rojgar.ui.base.BaseFragment
import com.nic.rojgar.ui.handleApiError
import com.nic.rojgar.ui.startNewActivity
import com.nic.rojgar.ui.visible
import kotlinx.coroutines.launch

class LoginFragment : BaseFragment<LoginViewModel, LoginFragmentBinding, AuthRepository>() {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.progressbar.visible(false)

        var userPerfrence : UserPreferences = UserPreferences(requireContext())
        viewModel.loginResponse.observe(viewLifecycleOwner, Observer {
            binding.progressbar.visible(it is Resource.Loading)
            when (it) {
                is Resource.Success -> {
                        lifecycleScope.launch {
                            viewModel.saveUserProfile(it.value)
                            requireActivity().startNewActivity(MainActivity::class.java)
                            Log.e("Login Fragment", userPerfrence.getUserProfile().toString())
                        }
                }
                is Resource.Failure -> {
                    handleApiError(it) {
                        login()
                    }
                }

            }
        })


        binding.btnSubmit.setOnClickListener {
            login()
        }
        binding.textViewWithOtp.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.registrationFragment)
        }
    }

    private fun login() {
        val username = binding.etUsername.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()

        val source = "0"

//        val first = Checksum(password.toByteArray())
//        val second = first.toString().uppercase() + source
//        val third = Checksum(second.toByteArray())
//        val final = third.toString().uppercase()

        // TODO: 07-09-2021 validations
        viewModel.login(username, password, source)
//            viewModel.login(username, password)
        binding.progressbar.visible(true)
    }


    override fun getViewModel() = LoginViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = LoginFragmentBinding.inflate(inflater, container, false)

    override fun getFragmentRepository() =
        AuthRepository(remoteDataSource.buildApi(AuthApi::class.java), userPreferences)

}