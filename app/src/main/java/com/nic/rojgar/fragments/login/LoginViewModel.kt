package com.nic.rojgar.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nic.ewam.data.network.Resource
import com.nic.rojgar.data.repository.AuthRepository
import com.nic.rojgar.data.response.UserInfoResponse
import com.nic.rojgar.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class LoginViewModel (
    private val repository: AuthRepository
) : BaseViewModel(repository) {
    private val _loginResponse: MutableLiveData<Resource<UserInfoResponse>> = MutableLiveData()

    val loginResponse: LiveData<Resource<UserInfoResponse>>
        get() = _loginResponse

    fun login(
        email: String,
        password: String,
        oneSignalId : String
    ) = viewModelScope.launch {
        _loginResponse.value = Resource.Loading
        _loginResponse.value = repository.login(email, password, oneSignalId)

    }

    suspend fun saveAuthToken(token: String) {
        repository.saveAuthToken(token)
    }

    suspend fun putString(key: String, value: String) {
        repository.putString(key, value)
    }

    suspend fun saveUserProfile(value: UserInfoResponse) {
        repository.saveUserProfile(value)
    }

}