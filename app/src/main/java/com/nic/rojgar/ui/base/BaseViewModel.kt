package com.nic.rojgar.ui.base

import androidx.lifecycle.ViewModel
import com.nic.rojgar.data.repository.BaseRepository

/**
 * Created by Mormukut Singh on 21-09-2021.
 * Mail Id : mormukutsinghji@gmail.com
 */
abstract class BaseViewModel(
    private val repository: BaseRepository
) : ViewModel() {
//    suspend fun logout(api: AuthApi) = repository.logout(api)
}