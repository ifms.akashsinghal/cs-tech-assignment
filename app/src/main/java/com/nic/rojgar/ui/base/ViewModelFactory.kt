package com.nic.rojgar.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nic.rojgar.data.repository.*
import com.nic.rojgar.fragments.login.LoginViewModel
import com.nic.rojgar.fragments.login.RegistrationViewModel
import java.lang.IllegalArgumentException


/**
 * Created by Akash Singhal on 18-Nov-21.
 * Mail Id: akashsi126@gmail.com
 */
class ViewModelFactory(
    private val repository: BaseRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> LoginViewModel(
                repository as AuthRepository
            ) as T
            modelClass.isAssignableFrom(RegistrationViewModel::class.java) -> RegistrationViewModel(
                repository as RegistrationRepository
            ) as T
            else -> throw IllegalArgumentException("ViewModel Not Found")
        }
    }


}