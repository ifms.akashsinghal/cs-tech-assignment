package com.nic.rojgar.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import com.nic.ewam.data.network.AuthApi
import com.nic.ewam.data.network.RemoteDataSource
import com.nic.rojgar.activity.AuthActivity
import com.nic.rojgar.data.UserPreferences
import com.nic.rojgar.data.repository.BaseRepository
import com.nic.rojgar.ui.startNewActivity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch


/**
 * Created by Akash Singhal on 18-Nov-21.
 * Mail Id: akashsi126@gmail.com
 */
abstract class BaseFragment<VM : BaseViewModel, B : ViewBinding, R : BaseRepository> : Fragment() {

    protected lateinit var binding: B
    protected lateinit var viewModel: VM
    protected lateinit var userPreferences: UserPreferences
    protected val remoteDataSource = RemoteDataSource()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userPreferences = UserPreferences(requireContext())
        binding = getFragmentBinding(inflater, container)
        val fatory = ViewModelFactory(getFragmentRepository())
        viewModel = ViewModelProvider(this, fatory).get(getViewModel())

        return binding.root
    }

    fun logout() = lifecycleScope.launch {
        val authToken = userPreferences.accessToken.first()
        val api = remoteDataSource.buildApi(AuthApi::class.java, authToken)
//        viewModel.logout(api)
        userPreferences.clear()
        requireActivity().startNewActivity(AuthActivity::class.java)
    }

    abstract fun getViewModel(): Class<VM>

    abstract fun getFragmentBinding(inflater: LayoutInflater, container: ViewGroup?): B

    abstract fun getFragmentRepository(): R

}