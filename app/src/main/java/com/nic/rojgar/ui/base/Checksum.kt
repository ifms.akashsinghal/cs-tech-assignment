package com.nic.rojgar.ui.base

import java.security.MessageDigest


/**
 * Created by Akash Singhal on 18-Nov-21.
 * Mail Id: akashsi126@gmail.com
 */
class Checksum (private val data: ByteArray)  {
    private val algorithm = "SHA-512"

    private fun generateChecksum(): String {
        val digest = MessageDigest.getInstance(algorithm)
        return printableHexString(digest.digest(data))
    }

    private fun printableHexString(digestedHash: ByteArray): String {
        return digestedHash.map { Integer.toHexString(0xFF and it.toInt()) }
            .map { if (it.length < 2) "0$it" else it }
            .fold("", { acc, d -> acc + d })
    }

    override fun toString(): String {
        return generateChecksum()
    }
}